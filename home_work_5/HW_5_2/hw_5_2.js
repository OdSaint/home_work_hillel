'use strict'

function f(a, b, c) {
  if(a == undefined && b == undefined){
      a = 2;
      b = 3;
  }

  var result = sum(a, b);

  if(typeof f === typeof c){
      return result = c();
  }

  return result;

  function sum(a, b) {
      return a + b;
  }
}

console.log(f(4, 6, function(){
return true;
}));