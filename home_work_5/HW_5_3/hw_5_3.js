'use strict'

var products = {};
var flag = true;


function getItem () {
var itemObj = {};

 var index = prompt('Введите индекс товара от 0 до 10');
 var productName = prompt('Введите название товара');
 var thePriceOfProduct = +prompt('Введите цену на товар');
 var category = prompt('Введите категорию товара');

 if(index && productName && thePriceOfProduct) {
  itemObj.productName = productName;
  itemObj.thePriceOfProduct = thePriceOfProduct;
  itemObj.category = category;  
  products[index] = itemObj;  
  return true;
 }
 return false;
}
while (flag) {
 flag = getItem();
}

function filterCategory(category) {
    var filter = {};
    category = prompt("Введите категорию для фильтрования");
    for( var key in products){
        if( products[key].category == category){
            filter[key] = products[key];
        }
    }
    
    return filter;
}

function specificСategory(category) {
    var discount = 12;
    var sumItems = 0;
    category = prompt("Выберите категорию для вычисления суммы товаров в ней?");
    for( var key in products){
        if( products[key].category == category){
            sumItems += products[key].thePriceOfProduct;
        }
    }
    
    if(sumItems === 0 || sumItems < 5){
        alert('Сумма товаров меньше 5, скидку вы не получаете.');
    } else if(sumItems > 5){
        sumItems = sumItems - (sumItems * discount/100);
        alert('Вы получили скидку в  12%');
    }
    return sumItems;
}



console.log(products);
console.log(filterCategory());
console.log(specificСategory())