'use strict'

function scraping(part, chapter, style){
    var parts = document.querySelectorAll('.list');
    var chapters = document.querySelectorAll('.list__item');
    var paragraphs = document.querySelectorAll('.list-sub__link');
    var partCont = [];
    var designTheme = {};
    var chapterCounter = count(1);
    var pointCounter = count(1);
	part--;
	chapter--;

    if(part < 0 || part > parts.length-1){
        console.log("Введите часть от 1 до ${parts.length}");
        return;
    }

    for(var i = 0; i  <parts.length; i++) {
		partCont[i] = [];
		for(var k = 0; k < chapters.length; k++) {
			if(parts[i].contains(chapters[k])) {
				partCont[i].push(chapters[k]);
			};
		};
    };
 
	if(chapter < 0 || chapter > partCont[part].length-1) {
		console.log(`Введите главу от 1 до ${partCont[part].length}`);
		return;
	}
    for(var i = 0; i < paragraphs.length; i++) {
		if(partCont[part][chapter].contains(paragraphs[i])) {
			(style === 'number' || style === 'current')? designTheme[styleSelection(style, chapterCounter(), chapter)] = `${paragraphs[i].textContent}`:
			designTheme[`${pointCounter()}`] = `${styleSelection(style, chapterCounter(), chapter)} ${paragraphs[i].textContent}`;
		};
    };
    for (var i in designTheme){
        console.log(i + '.', designTheme[i]);
    }

    //console.log(designTheme);
    //console.table(designTheme);
};

function styleSelection(style, callback, chapter){
    var styles;
    chapter ++;

    switch(style){
        case 'number':
            styles = callback;
        break;
        case 'dash':
            styles = '-';
        break;
        case 'current':
            styles = chapter + '.' + callback;       
    }
    return styles;
}
function count(startCount) {
	var counter = startCount;
	return function() {
		return counter++;
	};
};


scraping(2, 2, 'number');



