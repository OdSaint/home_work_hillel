function primeNumber(n) {
  if (n < 2) return false;

  var q = Math.floor(Math.sqrt(n));

  for (let i = 2; i <= q; i++) {
    if (n % i === 0) {
      return false;
    }
  }

  return true;
}

var n = +prompt('Введите число');
var prime = primeNumber(n);
if (prime) {
  console.log('prime');
} else {
  console.log('is not prime');
}