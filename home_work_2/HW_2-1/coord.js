'use strict'
var circleRadius = 5;
var sideOfSquare = 10;
var x = +prompt("Введите координату X");
var y = +prompt("Введите координату Y");

var coord = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

if (isNaN(x) || isNaN(y)) {
	alert('Координаты x и y должны быть числовыми');
}else if (x > 0 && y > 0 && x < sideOfSquare && y < sideOfSquare && coord > circleRadius) {
	alert("Точка находится в заштрихованной области");
}else {
	alert("Точка не попадает в заштрихованную область");
}

