//Пользователь вводит в prompt() числа через запятую (один раз много цифр, например: "54, 45, 53453, 7, 32").
//- Разрешите пользователю вводить минимум три числа, максимум - десять.
//- Разрешите вводить только числа меньше ста.
//- Если всё введено верно, отсортируйте числа от меньшего к большему и выведите в консоль новую строку.

'use strict'

function taskOne() {
    var input = prompt("Введите числа через запятую");
    var list = input.split(",");
    console.log(list);

    if (list.length < 3 || list.length > 10) {
        alert("Количество чисел должно быть больше 3 и <= 10");
        return;
      }

      for (var i = 0; i < list.length; i++) {
        var item = list[i];
        var num = parseInt(item);
    
        if (isNaN(num) || num > 100) {
          alert("число должно быть меньше ста");
          return;
        }
        list[i] = num;
    }
  
    list.sort(function(a, b) {
      return a - b;
    });
    console.log(list.join(", "));
  }
  taskOne();