//Есть массив [3,, 6,,, 7,, undefined, 34,,,, 97,, 43].
//- Пользователь вводит индекс. Ответьте есть ли элемент c таким индексом и, если есть, выведите его в консоль. Так же перенесите его (элемент) в конец массива.

'use strict'

function taskTwo() {
    var arr = [3, 6, 7, undefined, 10, 35, 67];
    var idx = +prompt("Введите индекс");

    if (idx < arr.length && arr[idx]){
        console.log('Элемент существует');
        console.log(arr[idx]);

        var val = arr[idx];
        arr.splice(idx, 1);
        arr.push(val);
		console.log(arr);
    }else {
        console.log('Элемента с таким индексом - Нет!');
    }
}
taskTwo();