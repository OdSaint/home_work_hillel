'use strict'

var goods = [{
    name: 'apricots',
    price: 140,
    category: 'fruits'
}, {
    name: 'almond',
    price: 900,
    category: 'nuts'
}, {
    name: 'pear',
    price: 90,
    category: 'fruits'
}, {
    name: 'tuna',
    price: 300,
    category: 'fish'
}, {
    name: 'salmon',
    price: 180,
    category: 'fish'
}, {
    name: 'herring',
    price: 120,
    category: 'fish'
}, {
    name: 'hazelnut',
    price: 600,
    category: 'nuts'
}, {
    name: 'apple',
    price: 80,
    category: 'fruits'
}, {
    name: 'plum',
    price: 70,
    category: 'fruits'
}, {
    name: 'mango',
    price: 350,
    category: 'fruits'
}];
//Напишите функцию, которая фильтрует товары по цене от и до и возращает новый массив только с товарами выбранного ценового диапазона или пустой массив.
function priceFromAndTo(from, to, arr) {
    return arr.filter(function (el) {
        return el.price >= from && el.price <= to;
    });
};
console.log(priceFromAndTo(200, 800, goods));

//Напишите функцию, которая фильтрует товары по категории и возращает новый массив только с товарами выбранной категории, если она есть или пустой массив.

var getCategory = function getCategory(categoryName, arr) {
    return arr.filter(function (el) {
      return el.category == categoryName;
    });
  };
console.log(getCategory('nuts', goods));

//Напишите функцию, которая возвращает количесто товаров в категории.
var getNumGoodsInCategory = function getNumGoodsInCategory(categoryName, arr) {
    return arr.reduce(function (sum, el) {
      return el.category == categoryName ? ++sum : sum;
    }, 0);
  };
  
  console.log(getNumGoodsInCategory('fruits', goods));

  //Напишите функцию, которая удаляет товар по имени.
  var deleteProduct = function deleteProduct(arr, productName) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i].name == productName) {
        arr.splice(i, 1);
        return true;
      }
    }
  
    return false;
  };
  
  deleteProduct(goods, 'hazelnut');
  console.log(goods);

//Напишите функции, которые сортируют товары по цене от меньшего к большему и наоборот и возвращают новый массив.
//аргумент sortingMark - если 1, то сортирует по возрастанию, если -1 по убыванию

var sortByPrice = function sortByPrice(arr, sortingMark) {
    return arr.slice("").sort(function(a, b) {
      return Number(a.price) > Number(b.price)
        ? 1 * sortingMark
        : Number(a.price) < Number(b.price)
          ? -1 * sortingMark
          : 0;
    });
  };
  
  console.log(sortByPrice(goods, 1));
  console.log(sortByPrice(goods, -1));

//Напишите функцию, котрая принимает вид сортировки (от большего к меньшему или наоборот) и фильтра (диапазон цены или категория) и возвращает новый массив товаров определённой выборки, отсортированные как указал пользователь.
//sortingMark - поставьте 1 для сортировки по возрастанию или -1 по убыванию
//...rest - либо категорию в виде строки в кавычках, например 'fruits', либо два числа через запятую, например 200, 630

var selectAndSort = function selectAndSort(arr, sortingMark) {
    var buff;
    if ((arguments.length <= 2 ? 0 : arguments.length - 2) == 2)
      buff = priceFromAndTo(
        arguments.length <= 2 ? undefined : arguments[2],
        arguments.length <= 3 ? undefined : arguments[3],
        arr
      );
    else if ((arguments.length <= 2 ? 0 : arguments.length - 2) == 1)
      buff = getCategory(arguments.length <= 2 ? undefined : arguments[2], arr);
    return sortByPrice(buff, sortingMark);
  };
  
  console.log(selectAndSort(goods, -1, 100, 900));
  console.log(selectAndSort(goods, 1, "fruits"));

  //Напишите функцию, котрая принимает фильтра (диапазон цены или категория) и возвращает сумму цен товаров этой выборки.

  var selectAndSum = function selectAndSum(arr) {
    var buff;
    if ((arguments.length <= 1 ? 0 : arguments.length - 1) == 2)
      buff = priceFromAndTo(
        arguments.length <= 1 ? undefined : arguments[1],
        arguments.length <= 2 ? undefined : arguments[2],
        arr
      );
    else if ((arguments.length <= 1 ? 0 : arguments.length - 1) == 1)
      buff = getCategory(arguments.length <= 1 ? undefined : arguments[1], arr);
    return buff.reduce(function(sum, el) {
      return sum + el.price;
    }, 0);
  };
  
  console.log(selectAndSum(goods, 50, 200));
  console.log(selectAndSum(goods, "fish"));